package com.citi.training.trades.rest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.trades.model.Trade;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("h2")
public class TradeControllerIntegrationTest {
	
	@Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void getStock_returnsStock() {
    	int id = 1;
    	String stock = "AAPL";
    	int buy = 1;
    	double price = 150.00;
    	int volume = 10;

        ResponseEntity<Trade> createTradeResponse = restTemplate.postForEntity("/trades",
                                             new Trade(id, stock, buy, price, volume),
                                             Trade.class);

        assertEquals(HttpStatus.CREATED, createTradeResponse.getStatusCode());
        assertEquals(stock, createTradeResponse.getBody().getStock());
        assertEquals(buy, createTradeResponse.getBody().getBuy());
        assertEquals(price, createTradeResponse.getBody().getPrice(), 0.0001);
        assertEquals(volume, createTradeResponse.getBody().getVolume());
        
        
        ResponseEntity<Trade> getTrade = restTemplate.getForEntity("/trades/" 
        + createTradeResponse.getBody().getId(), Trade.class);
        
        assertEquals(id, getTrade.getBody().getId());
        assertEquals(HttpStatus.OK, getTrade.getStatusCode());
        
        
        ResponseEntity<Trade> getFake = restTemplate.getForEntity("/trades/" + 99,
        		Trade.class);
        
        assertEquals(HttpStatus.NOT_FOUND, getFake.getStatusCode());

        
        ResponseEntity<Trade> getTradeDeletedHttp = restTemplate.exchange("/trades/" 
                + createTradeResponse.getBody().getId(), HttpMethod.DELETE,
                null, Trade.class);
        
        assertEquals(HttpStatus.NO_CONTENT, getTradeDeletedHttp.getStatusCode());
        
        
        ResponseEntity<Trade> getTradeDeletedHttpFake = restTemplate.exchange("/trades/" 
                + 999, HttpMethod.DELETE,
                null, Trade.class);
        
        assertEquals(HttpStatus.NOT_FOUND, getTradeDeletedHttpFake.getStatusCode());
        
        
        restTemplate.delete("/trades/" + getTrade.getBody().getId());
        
        ResponseEntity<Trade> getTradeDeleted = restTemplate.getForEntity("/trades/" 
                + createTradeResponse.getBody().getId(), Trade.class);
        
        assertEquals(HttpStatus.NOT_FOUND, getTradeDeleted.getStatusCode());
    }
}
