package com.citi.training.trades.rest;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.citi.training.trades.model.Trade;
import com.citi.training.trades.dao.TradeDao;
import com.citi.training.trades.rest.TradeController;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@WebMvcTest(TradeController.class)
public class TradeControllerTest {
	
    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TradeDao tradeDao;
    
    
    private int id = 1;
	private String stock = "AAPL";
	private int buy = 1;
	private double price = 150.00;
	private int volume = 10;
    
    @Test
    public void findAllTrades_returnsList() throws Exception {
        when(tradeDao.findAll()).thenReturn(new ArrayList<Trade>());

        MvcResult result = this.mockMvc.perform(get("/trades")).andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").isNumber()).andReturn();
    }
    
    @Test
    public void getStockById_returnsOK() throws Exception {
        Trade testTrade = new Trade(id, stock, buy, price, volume);

        when(tradeDao.findById(testTrade.getId())).thenReturn(
                                                    Optional.of(testTrade));

        MvcResult result = this.mockMvc.perform(get("/trades/" + testTrade.getId()))
                                       .andExpect(status().isOk()).andReturn();
    }

}
