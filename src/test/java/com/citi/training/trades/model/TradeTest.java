package com.citi.training.trades.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.citi.training.trades.model.Trade;

public class TradeTest {
	
	private int id = 1;
	private String stock = "AAPL";
	private int buy = 1;
	private double price = 150.00;
	private int volume = 10;
	
	@Test
    public void test_Trade_fullConstructor() {
        Trade testTrade = new Trade(id, stock, buy, price, volume);

        assertEquals(id, testTrade.getId());
        assertEquals(stock, testTrade.getStock());
        assertEquals(buy, testTrade.getBuy());
        assertEquals(price, testTrade.getPrice(), 0.0001);
        assertEquals(volume, testTrade.getVolume());
    }
	
	@Test
    public void test_Trade_fullConstructorDifferentValues() {
        Trade testTrade = new Trade(id, "GOOGL", 0, 20.00, 30);

        assertEquals(id, testTrade.getId());
        assertEquals("GOOGL", testTrade.getStock());
        assertEquals(0, testTrade.getBuy());
        assertEquals(20.00, testTrade.getPrice(), 0.0001);
        assertEquals(30, testTrade.getVolume());
    }
	
	@Test(expected = IllegalArgumentException.class)
    public void test_Trade_BuySetter() {
        Trade testTrade = new Trade(id, stock, 9999, price, volume);
    }
	
	@Test(expected = IllegalArgumentException.class)
    public void test_Trade_BuySetterNegative() {
        Trade testTrade = new Trade(id, stock, -9999, price, volume);
    }
	
	@Test(expected = IllegalArgumentException.class)
    public void test_Trade_PriceSetter() {
        Trade testTrade = new Trade(id, stock, buy, -100, volume);
    }
	
	@Test(expected = IllegalArgumentException.class)
    public void test_Trade_VolumeSetter() {
        Trade testTrade = new Trade(id, stock, buy, price, -100);
    }
	
	@Test
    public void test_Trade_toString() {
		Trade testTrade = new Trade(id, stock, buy, price, volume);

        assertTrue("toString should contain id",
        		testTrade.toString().contains(Integer.toString(id)));
        assertTrue("toString should contain stock",
        		testTrade.toString().contains(stock));
        assertTrue("toString should contain buy",
        		testTrade.toString().contains(Integer.toString(buy)));
        assertTrue("toString should contain price",
        		testTrade.toString().contains(Double.toString(price)));
        assertTrue("toString should contain volume",
        		testTrade.toString().contains(Integer.toString(volume)));
    }

}
