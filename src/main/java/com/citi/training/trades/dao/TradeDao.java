package com.citi.training.trades.dao;

import org.springframework.data.repository.CrudRepository;

import com.citi.training.trades.model.Trade;

/**
 * 
 * @author Ivaylo
 *
 * This is the Trade Data Access Object
 */

public interface TradeDao extends CrudRepository<Trade, Integer> {

}
