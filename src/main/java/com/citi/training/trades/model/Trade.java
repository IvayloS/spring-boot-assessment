package com.citi.training.trades.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * 
 * @author Ivaylo
 *
 * This is the Trade class
 */

@Entity
public class Trade {
	
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(length=50)
	private String stock;
	
	private int buy;
	private double price;
	private int volume;
	
	public Trade() {}
	
	public Trade(int id, String stock, int buy, double price, int volume) {
		
		this.setId(id);
		this.setStock(stock);
		this.setBuy(buy);
		this.setPrice(price);
		this.setVolume(volume);
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getStock() {
		return stock;
	}


	public void setStock(String stock) {
		this.stock = stock;
	}


	public int getBuy() {
		return buy;
	}


	public void setBuy(int buy) {
		
		if (buy != 0 && buy != 1) {
			throw new IllegalArgumentException("Buy should be an integer value - either 1 or 0");
		} else {
			this.buy = buy;
		}
	}


	public double getPrice() {
		return price;
	}


	public void setPrice(double price) {
		if (price < 0) {
			throw new IllegalArgumentException("Price should be positive.");
		} else {
			this.price = price;
		}
	}


	public int getVolume() {
		return volume;
	}


	public void setVolume(int volume) {
		if (volume < 0) {
			throw new IllegalArgumentException("Volume should be positive.");
		} else {
			this.volume = volume;
		}
		
	}


	@Override
	public String toString() {
		return "Trade [id=" + id + ", stock=" + stock + ", buy=" + buy + ", price=" + price + ", volume=" + volume
				+ "]";
	}
	
}
